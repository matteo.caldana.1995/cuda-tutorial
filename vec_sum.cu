#include <iostream>
#include <math.h>

__global__ void add01(int n, float *x, float *y) {
  for (int i = 0; i < n; i++)
    y[i] = x[i] + y[i];
}

__global__ void add02(int n, float *x, float *y) {
  int index = threadIdx.x;
  int stride = blockDim.x;
  for (int i = index; i < n; i += stride)
      y[i] = x[i] + y[i];
}

__global__ void add03(int n, float *x, float *y) {
  int tid = blockIdx.x * blockDim.x + threadIdx.x;
  // Handling arbitrary vector size
  if (tid < n){
      y[tid] = x[tid] + y[tid];
  }
}

int main(int argc, char **argv) {
  int N = 1<<20;
  float *x, *y;

  // Allocate Unified Memory – accessible from CPU or GPU
  cudaMallocManaged(&x, N*sizeof(float));
  cudaMallocManaged(&y, N*sizeof(float));

  // initialize x and y arrays on the host
  for (int i = 0; i < N; i++) {
    x[i] = 1.0f;
    y[i] = 2.0f;
  }

  switch(argv[1][0]) {
    case '1': {
      add01<<<1, 1>>>(N, x, y);
      break;
    }
    case '2': {
      add02<<<1, 256>>>(N, x, y);
      break;
    }
    case '3': {
      int blockSize = 256;
      int numBlocks = (N + blockSize - 1) / blockSize;
      add03<<<numBlocks, blockSize>>>(N, x, y);
      break;
    }
    default:
      std::cout << "ERROR: unknown option " << argv[1][0] << std::endl;
  }

  // Wait for GPU to finish before accessing on host
  cudaDeviceSynchronize();

  // Check for errors (all values should be 3.0f)
  float maxError = 0.0f;
  for (int i = 0; i < N; i++)
    maxError = fmax(maxError, fabs(y[i]-3.0f));
  std::cout << "Max error: " << maxError << std::endl;

  // Free memory
  cudaFree(x);
  cudaFree(y);
  
  return 0;
}