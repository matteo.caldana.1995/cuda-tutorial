# Compliling CUDA on Colab

```
!nvcc --version
!git clone https://gitlab.com/matteo.caldana.1995/cuda-tutorial.git
```

```
!cd cuda-tutorial && git pull
!nvcc cuda-tutorial/vec_sum.cu -o vec_sum
!nvprof ./vec_sum 1
```